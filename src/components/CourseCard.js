import {useState, useEffect} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard({courseProp}){
	// console.log(props)
	/*console.log(courseProp)	*//*objects*/


	/*object destructuring*/
	const {name, description, price, id, onOffer, sample} = courseProp

	// console.log(name)
	// console.log(description)
	// console.log(price)
	// console.log(id)
	// console.log(onOffer)
	// console.log(sample)	//returns undefined


	// const arr = ["one", "two", "three", "four"]

	/*array destructuring*/
	// const [e1, e2, , e4] = arr
	// console.log(e1)
	// console.log(e2)
	// console.log(e4)

	/*review of useState hook*/
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(10);

	const [isDisabled, setIsDisabled] = useState(false)

	function enroll(){
		setCount(count + 1)
		setSeats(seats - 1)
	}

	useEffect( () => {

		if(seats === 0){
			setIsDisabled(true)
		}

	}, [seats] )

	return(
		<Container fluid>
			<Row className="mb-3">
				<Col>
					<Card>
					  <Card.Body>
					    <Card.Title>{name}</Card.Title>
					    <Card.Subtitle>Description:</Card.Subtitle>
					    <Card.Text>
					      {description}
					    </Card.Text>
					    <Card.Subtitle>Price:</Card.Subtitle>
					    <Card.Text>
					      {price}
					    </Card.Text>

					    <Card.Text>Enrollees: {count}</Card.Text>
					    <Card.Text>Seats: {seats}</Card.Text>
					    <Button 
					    	variant="primary"
					    	onClick={enroll}
					    	disabled={isDisabled}
					    >Enroll
					    </Button>
					  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}